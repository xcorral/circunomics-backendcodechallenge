- How were you debugging this mini-project? Which tools?

I use Xdebug to debug php projects, together with Phpstorm configured to listen to the xdebug requests. Also, the symfony logger is a useful tool (in dev mode, you may find logs under /var/log/dev)

- How were you testing the mini-project?

In this case, running the command to download the commits, and checking against the original repo. With more time, I would have implemented some unit tests on the Service, as well as some E2E and acceptance behat tests 

- Imagine this mini-project needs microservices with one single database, how would you draft an architecture?

I would set up a DDD architecture, with a single context (Commits), and two application cases, SaveCommitsApplicationCase and FindCommitsApplicationCase, having the infrastructure details (api connection, doctrine repo) in a separate folder, all implementing contract interfaces in the Model folder. For Model files, I would create an Aggregate root entity 'Commit' having all of its fields modelled as Value Objects 

- How would your solution differ when all over the sudden instead of saving to a Database you would have to call another external API to store and receive the commits.

In the infrastructure implementation of the GitService, I would implement the required contract method (downloadCommits) having all the dependencies defined in the service container. For this first approach, I have defined the service in the DI container (services.yaml) as 'App\Service\GithubService'