<?php

namespace App\Command;

use App\Service\GithubService;
use App\Service\GitService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DownloadLastCommitsCommand extends Command
{
    CONST NUMBER_OF_COMMITS = 25;

    protected static $defaultName = 'app:download-last-commits';
    protected static $defaultDescription = 'Download latest commits';
    private EntityManagerInterface $entityManager;
    private GitService $gitService;

    public function __construct(EntityManagerInterface $entityManager, GitService $gitService)
    {
        $this->entityManager = $entityManager;
        $this->gitService = $gitService;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription(self::$defaultDescription);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $commits = $this->gitService->downloadCommits(self::NUMBER_OF_COMMITS);

        $this->fillCommitsEntitiesFromArray($commits);

        $io->success('Latest commits downloaded!');

        return 0;
    }

    private function fillCommitsEntitiesFromArray(array $commits): void
    {
        if (count($commits) === 0) {
            return;
        }

        foreach ($commits as $commit) {
            try {
                $this->saveCommit($commit);
            } catch (\Exception $e) {
                echo $e->getMessage();die();
            }
        }
    }

    private function saveCommit(object $commit): void
    {
        $query = <<<SQL
INSERT IGNORE INTO
    commits
SET
    hash = :hash,
    description = :description,
    author = :author,
    created_at = :created_at
SQL;
        $statement = $this->entityManager->getConnection()->prepare($query);

        $statement->bindValue('hash', $commit->sha);
        $statement->bindValue('description', substr($commit->commit->message, 0, strpos($commit->commit->message, "\n")));
        $statement->bindValue('author', $commit->commit->author->name);
        $statement->bindValue('created_at', $commit->commit->committer->date);

        $statement->execute();
    }
}
