<?php

declare(strict_types=1);

namespace App\Service;

final class GithubService implements GitService
{
    const BASE_URL = 'https://api.github.com/repos/%s/%s/commits?per_page=%d';
    const USER_AGENT = 'User-Agent: Circunomics-Commits-App';

    private string $githubUser;
    private string $repositoryName;

    public function __construct(
        string $githubUser,
        string $repositoryName
    ) {
        $this->githubUser = $githubUser;
        $this->repositoryName = $repositoryName;
    }

    public function downloadCommits(int $numberOfCommits = self::DEFAULT_NUMBER_OF_COMMITS): array
    {
        $githubUrl = sprintf(self::BASE_URL, $this->githubUser, $this->repositoryName, $numberOfCommits);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $githubUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [self::USER_AGENT]);
        curl_setopt($ch, CURLOPT_URL, $githubUrl);
        $commits = curl_exec($ch);

        return json_decode($commits);
    }
}