<?php

declare(strict_types=1);

namespace App\Service;

interface GitService
{
    CONST DEFAULT_NUMBER_OF_COMMITS = 25;

    public function downloadCommits(int $numberOfCommits = self::DEFAULT_NUMBER_OF_COMMITS): array;
}