<?php

namespace App\Controller;

use App\Entity\Commit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommitsController extends AbstractController
{
    /**
     * @Route("/commits", name="commits")
     */
    public function index(): Response
    {
        $commits = $this->getDoctrine()
            ->getRepository(Commit::class)
            ->findByAuthor();

        return $this->render('commits/index.html.twig', [
            'commits' => $commits,
        ]);
    }
}
