import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppHeader } from "./components/app.header";
import {AppHeaderText} from "./components/app.header-text";

@NgModule({
    imports: [
        BrowserModule
    ],
    declarations: [
        AppHeader,
        AppHeaderText
    ],
    bootstrap: [ AppHeader ]
})
export class AppModule { }
