// Import the native Angular services.
import { Component } from '@angular/core';

@Component({
    selector: 'app-header',
    template: `
        <header>
            <h2>Commits from NodeJs ✅</h2>
            <app-header-text [numberOfCommits]="numberOfCommits"></app-header-text>
        </header>  
    `,
})

export class AppHeader {
    private numberOfCommits: number = 25;
    public constructor() { }
}
