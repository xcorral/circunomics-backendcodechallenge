// Import the native Angular services.
import { Component , OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
    selector: 'app-header-text',
    template: `
        <p>These are the last {{ numberOfCommits }} commits ordered by author:</p>
    `,
})

export class AppHeaderText implements OnInit {
    @Input('numberOfCommits') numberOfCommits: number|undefined;

    public constructor() { }

    ngOnInit() {
    }
}
