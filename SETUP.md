# Xavi Corral Backend Coding Challenge

## Implement Commit Downloader:

## 🚀 Environment setup

### 🐳 Needed tools

1. [Install Docker](https://www.docker.com/get-started)
2. [Install Composer](https://getcomposer.org/download/)
3. [Install Yarn](https://classic.yarnpkg.com/en/docs/install/#mac-stable)
4. Clone this project: `git clone https://bitbucket.org/xcorral/circunomics-backendcodechallenge`
5. Move to the project folder: `cd circunomics-backendcodechallenge`
6. Install dependencies: `composer install`
7. Install node_modules: `yarn install`
8. Launch webpack: `yarn encore dev`
9. Bring up the project Docker containers with Docker Compose: `docker-compose up -d`
10. Launch the command to download the last 25 commits: `docker exec circunomics_php_fpm bin/console app:download-last-commits`

### 🌍 Application execution

Go to [the last NodeJs commits](http://localhost:8000/commits)

Database tables should be created the first time the mysql container is created. In case of troubles, you may find a snapshot of the structure at /database/init.sql
